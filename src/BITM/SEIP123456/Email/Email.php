<?php
namespace App\Email;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Email extends DB{
    public $id= "";
    public $name= "";
    public $email_address= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('name', $post)){
            $this->name = $post['name'];
        }
        if (array_key_exists('email_address', $post)){
            $this->email_address = $post['email_address'];
        }

    }
    public function store(){
        $arrData = array( $this->name, $this->email_address);

        $sql = "Insert INTO email(name, email_address) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

}
