<?php
namespace App\Gender;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Gender extends DB{
    public $id= "";
    public $name= "";
    public $gender= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('name', $post)){
            $this->name = $post['name'];
        }
        if (array_key_exists('gender', $post)){
            $this->gender = $post['gender'];
        }

    }
    public function store(){
        $arrData = array( $this->name, $this->gender);

        $sql = "Insert INTO gender(name, gender) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

}
