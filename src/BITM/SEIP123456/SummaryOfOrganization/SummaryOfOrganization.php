<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class SummaryOfOrganization extends DB{
    public $id= "";
    public $organization= "";
    public $summary= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('organization', $post)){
            $this->organization = $post['organization'];
        }
        if (array_key_exists('summary', $post)){
            $this->summary = $post['summary'];
        }

    }
    public function store(){
        $arrData = array( $this->organization, $this-> summary);

        $sql = "Insert INTO summary_of_organization(organization, summary) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

}
