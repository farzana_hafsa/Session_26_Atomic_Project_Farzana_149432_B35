<?php
namespace App\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class ProfilePicture extends DB{
    public $id= "";
    public $name= "";
    public $profile_picture= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('name', $post)){
            $this->name = $post['name'];
        }
        if (array_key_exists('file', $post)){
            $this->profile_picture = $post['file'];
        }

    }
    public function store(){
        $path='C:\xampp\htdocs\LabExam7_Atomic_Project_Farzana_149432_B35\resource\Upload';
        $upload= $path.basename($this->profile_picture);
        move_uploaded_file($_FILES['profile_picture']['tmp_name'],$upload);
        $arrData = array( $this->name, $this->profile_picture);

        $sql = "Insert INTO profile_picture(name, profile_picture) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

}
